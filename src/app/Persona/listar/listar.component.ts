import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from 'src/app/Modelo/Persona';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

 
  constructor(private service:ServiceService,private router:Router) {
   }
  
   personas!:Persona[];
  ngOnInit(): void {
   this.service.getPersonas()
   .subscribe(data=>{
    this.personas=data;
   });
  }

  Editar(persona:Persona){
    localStorage.setItem("id",persona.id.toString());
    this.router.navigate(["edit"]);
  }

  Cuentas(persona:Persona){
    localStorage.setItem("idPersona",persona.id.toString());
    this.router.navigate(["lsCuenta"]);
  }
  
  listarCuenta(persona:Persona){

  }
}

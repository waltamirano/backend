import { Persona } from "./Persona";

export class Cuenta{
     id!:number;
     persona!:Persona;
     cuentaNumero!:String;
     moneda!:number;
}
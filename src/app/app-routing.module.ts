import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCuentaComponent } from './Cuenta/add-cuenta/add-cuenta.component';
import { ListComponent } from './Cuenta/list/list.component';
import { AddComponent } from './Persona/add/add.component';
import { EditComponent } from './Persona/edit/edit.component';
import { ListarComponent } from './Persona/listar/listar.component';

const routes: Routes = [
  {path:"listar",component:ListarComponent},
  {path:"add",component:AddComponent},
{path:"edit",component:EditComponent},
{path:"lsCuenta",component:ListComponent},
{path:"addCuenta",component:AddCuentaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

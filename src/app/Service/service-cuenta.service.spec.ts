import { TestBed } from '@angular/core/testing';

import { ServiceCuentaService } from './service-cuenta.service';

describe('ServiceCuentaService', () => {
  let service: ServiceCuentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceCuentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

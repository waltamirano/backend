import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cuenta } from '../Modelo/Cuenta';

@Injectable({
  providedIn: 'root'
})
export class ServiceCuentaService {

  constructor(private http:HttpClient) { }

  Url='http://localhost:8080/banco-api/cuenta/'

  getCuentaId(id:number){
    return this.http.get<Cuenta[]>(this.Url+"/"+id);
  }

  agregarCuenta(cuenta:Cuenta){
    return this.http.post<Cuenta>(this.Url,cuenta);
  }
}

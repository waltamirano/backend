import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cuenta } from 'src/app/Modelo/Cuenta';
import { Persona } from 'src/app/Modelo/Persona';
import { ServiceCuentaService } from 'src/app/Service/service-cuenta.service';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-add-cuenta',
  templateUrl: './add-cuenta.component.html',
  styleUrls: ['./add-cuenta.component.css']
})
export class AddCuentaComponent implements OnInit {

  cuenta:Cuenta= new Cuenta();
  persona:Persona=new Persona();
  constructor(private router:Router, private service:ServiceCuentaService, private srvPersona:ServiceService) { }

  ngOnInit(): void {
    this.Persona();
  }

  
  Persona(){
    let id= localStorage.getItem("idPersona");
    this.srvPersona.getPersonaId(Number(id))
    .subscribe(data=>{
         this.persona=data;
    });
  }

  Guardar(){
    this.cuenta.persona=this.persona;
    this.service.agregarCuenta(this.cuenta)
    .subscribe(data=>{
      alert("La Cuenta fue Registrada");
      this.router.navigate(["lsCuenta"])
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cuenta } from 'src/app/Modelo/Cuenta';
import { CuentaVm } from 'src/app/Modelo/CuentaVm';
import { Persona } from 'src/app/Modelo/Persona';
import { ServiceCuentaService } from 'src/app/Service/service-cuenta.service';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  cuentas!:Cuenta[];
  persona:Persona=new Persona();
  constructor(private router:Router, private service:ServiceCuentaService,private servicePersona:ServiceService) { }

  ngOnInit(): void {
    this.Cuentas();
    this.Persona();
  }

  Persona(){
    let id= localStorage.getItem("idPersona");
    this.servicePersona.getPersonaId(Number(id))
    .subscribe(data=>{
         this.persona=data;
    });
  }

  Cuentas(){
    let id= localStorage.getItem("idPersona");
    this.service.getCuentaId(Number(id))
    .subscribe(data=>{
      this.cuentas=data;
      console.log(this.cuentas);
    })
  }

  AgregaCuenta(){
    this.router.navigate(["addCuenta"]);
  }
}
